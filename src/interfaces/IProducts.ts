export interface IProducts{
    id :string  ,
    productname : string,
    productsubfixname : string,
    brandname : string,
    producttype : string,
    productcost : number,
    productprice : number,
    currency : string,
    dateadd : string,
    isInStock : boolean 
}