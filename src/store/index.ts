import { createStore } from 'vuex'
import { IProducts } from '@/interfaces';

const localStorage =window.localStorage.getItem('products');
export default createStore({
  state: {
    products: localStorage == null || localStorage == '' ? [] : JSON.parse(localStorage)
  } as {
    products : IProducts[]
  },
  mutations: {
    addNewProduct(state,productModel : IProducts) : void{
      state.products.push(productModel);
      window.localStorage.setItem('products',JSON.stringify(state.products));
    },
    romoveOrUpdateProduct(state,newData : IProducts[]) : void{
      state.products = newData;
      window.localStorage.setItem('products',JSON.stringify(state.products));
    }
  },
  actions: {
  },
  modules: {
  },
  getters: {
    countProducts(state) : number{
      return state.products.length;
    }
  }
})
