import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './assets/css/product.css';

const app = createApp(App);
app.config.globalProperties.$store = store;
app.use(store)
app.use(router)
app.mount('#app')
